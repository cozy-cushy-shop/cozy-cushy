import { Controller, Get } from '@nestjs/common';
import { UsersService } from 'src/services/user.service';

@Controller('users')
export class UserController {
  constructor(private userService: UsersService) {}
  @Get()
  getUser() {
    return this.userService.getUser();
  }
}
