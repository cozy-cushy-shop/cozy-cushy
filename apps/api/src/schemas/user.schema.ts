import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type UserDocument = HydratedDocument<User>;

@Schema()
export class User {
  @Prop({
    required: true,
    unique: true,
    trim: true,
    type: String,
  })
  username: string;
  @Prop({
    required: true,
    trim: true,
    type: String,
  })
  password: string;
  @Prop({
    required: true,
    trim: true,
    type: String,
  })
  email: string;
}

export const UserSchema = SchemaFactory.createForClass(User);
