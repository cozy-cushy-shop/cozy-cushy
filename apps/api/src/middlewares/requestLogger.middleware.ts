// External dependencies
import { Injectable, NestMiddleware, Logger } from '@nestjs/common';
import { FastifyRequest, FastifyReply } from 'fastify';

@Injectable()
export class RequestLogger implements NestMiddleware {
  private readonly logger = new Logger(RequestLogger.name);

  use(req: FastifyRequest['raw'], res: FastifyReply['raw'], next: () => void) {
    this.logger.log(`Request started! ${req.method} ${req.url}`);

    res.on('finish', () => {
      const successful = res.statusCode >= 200 && res.statusCode < 300;

      this.logger.log(
        `Request finished ${successful ? 'successfully' : 'with errors'}! ${req.method} ${
          req.url
        } ${res.statusCode} ${res.statusMessage}`,
      );
    });

    next();
  }
}
