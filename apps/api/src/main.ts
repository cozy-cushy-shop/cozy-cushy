import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';

async function bootstrap() {
  const CORS_OPTIONS = {
    origin: [process.env.ADMIN_HOST, process.env.CLIENT_HOST || 'none'],
    allowedHeaders: [
      'Access-Control-Allow-Origin',
      'Origin',
      'X-Requested-With',
      'Accept',
      'Content-Type',
      'Authorization',
    ],
    methods: ['GET', 'PUT', 'OPTIONS', 'POST', 'PATCH', 'DELETE'],
  };
  const app: NestFastifyApplication =
    await NestFactory.create<NestFastifyApplication>(
      AppModule,
      new FastifyAdapter(),
    );
  app.enableCors(CORS_OPTIONS);

  await app.listen(Number(process.env.API_PORT));
}
bootstrap();
