import { InputHTMLAttributes } from "react";
import "./Checkbox.scss";

interface CheckBoxProps extends InputHTMLAttributes<HTMLInputElement> {}
function CheckBox(props: CheckBoxProps) {
  const { id, className, ...rest } = props;

  let checkboxClassName = "checkbox";

  if (className) {
    checkboxClassName += " " + className;
  }

  return (
    <label htmlFor={id} className={checkboxClassName}>
      <input
        type="checkbox"
        name=""
        id={id}
        className="checkbox__input"
        {...rest}
      />
    </label>
  );
}

export default CheckBox;
