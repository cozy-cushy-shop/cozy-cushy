import React, { useState, useRef, useEffect } from "react";
import TextField from "../TextField";
import "./index.scss";
import { Icon } from "@iconify/react";

export interface SelectBoxDataType {
  id: number;
  name: string;
}

interface SelectBoxProps {
  data: SelectBoxDataType[];
  onSelect: (selectedItem: SelectBoxDataType | null) => void;
  label?: string;
  error: boolean;
  helperText: string;
}

const SelectBox: React.FC<SelectBoxProps> = ({
  data,
  onSelect,
  label,
  error,
  helperText,
}: SelectBoxProps) => {
  const [isOpen, setIsOpen] = useState(false);
  const [filteredData, setFilteredData] = useState<SelectBoxDataType[]>(data);
  const inputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    const handleDocumentClick = (e: MouseEvent) => {
      if (inputRef.current && e.target !== inputRef.current) {
        setIsOpen(false);
      }
    };

    document.addEventListener("click", handleDocumentClick);
    return () => document.removeEventListener("click", handleDocumentClick);
  }, []);

  const handleChange = () => {
    const filterValue = inputRef.current?.value.toLowerCase() || "";
    const filtered = data.filter((item) =>
      item.name.toLowerCase().includes(filterValue),
    );
    setFilteredData(filtered);
    setIsOpen(true);
  };

  const handleSelectItem = (item: SelectBoxDataType) => {
    if (inputRef.current) {
      inputRef.current.value = item.name;
    }
    setIsOpen(false);
    onSelect(item);
    setFilteredData(data); // Reset the filtered data to the original list
  };

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const renderDropdown = () => {
    if (!isOpen) return null;

    const itemsToRender = filteredData.length > 0 ? filteredData : data;

    if (filteredData.length === 0 && inputRef.current?.value) {
      return (
        <div className="select-box__not-found">
          Result not found!
          <Icon icon="bx:search-alt" width={20} height={20} />
        </div>
      );
    }

    return itemsToRender.map((item) => (
      <div
        className="select-box__option"
        key={item.id}
        onClick={() => handleSelectItem(item)}
      >
        {item.name}
      </div>
    ));
  };

  return (
    <div className="select-box">
      <TextField
        ref={inputRef}
        label={label}
        onChange={handleChange}
        onClick={toggleDropdown}
        error={error}
        helperText={helperText}
      />
      <div
        className={`select-box__options ${isOpen ? "open" : ""} ${error ? "error" : ""}`}
      >
        {renderDropdown()}
      </div>
    </div>
  );
};

export default SelectBox;
