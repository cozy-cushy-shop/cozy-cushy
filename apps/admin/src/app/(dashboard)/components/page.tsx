"use client";

import { Icon } from "@iconify/react";
import { Component } from "components/shared/Components";
import Button from "components/ui/Button";
import CheckBox from "components/ui/Checkbox";
import ButtonIcon from "components/ui/IconButton";
import InputQuantity from "components/ui/Quantity";

import SelectBox, { SelectBoxDataType } from "components/ui/SelectBox";
import TextField from "components/ui/TextField";
import Switch from "components/ui/Switch";
import { animals } from "helpers/fakeData";

import InputRadio from "components/ui/InputRadio";
import React, { useState } from "react";
import Tabs, { DataTabItem } from "components/ui/Tabs";

export default function Page() {
  const handleFilter = (filteredData: SelectBoxDataType | null) => {
    console.log("Filtered data:", filteredData);
  };

  const tabData: DataTabItem[] = [
    { title: "All", id: 1 },
    { title: "In Stock", id: 2 },
    { title: "Out of Stock", id: 3 },
  ];

  const handleChangeTab = (tab: DataTabItem) => {
    console.log(tab);
  };
  return (
    <>
      <Component title="Button Component">
        <Button>send money</Button>

        <Button variant="outline">send money </Button>

        <Button variant="secondary">
          add product <Icon icon="ic:baseline-plus" />
        </Button>

        <Button className="ccc" size="large" variant="secondary">
          View Full Report
        </Button>

        <Button loading variant="secondary" style={{ width: 300 }}>
          Log in
        </Button>
      </Component>
      <Component title="Button Icon">
        <div>
          <ButtonIcon icon="tabler:bell-filled" disabled />
        </div>
        <div>
          <ButtonIcon icon="ep:menu" />
        </div>
        <div>
          <ButtonIcon icon="ri:search-2-line" />
        </div>
      </Component>
      <Component title="Checkbox">
        <CheckBox
          // onChange={updateRadio}
          // checked={value.includes('1')}
          value={"1"}
        />
        <CheckBox
          // onChange={updateRadio}
          // checked={value.includes('2')}
          value={"2"}
        />
      </Component>
      <Component title="Input Quantity">
        <InputQuantity quantity={1} onChangeQuantity={() => {}} />
      </Component>
      <Component title="TextField">
        <TextField label="TextField" />
        <TextField
          label="TextField"
          error
          helperText="This textfield is error"
        />
      </Component>

      <Component title="Switch">
        <Switch id="test" disabled={false} />
        <Switch id="test1" disabled={true} />
      </Component>
      <Component title="Select Box">
        <SelectBox
          data={animals}
          onSelect={handleFilter}
          label="Select Option"
          error={false}
          helperText="Please select a value"
        />
      </Component>
      <Component title="radio">
        <InputRadio
          // onChange={updateRadio}
          name="myRadio"
          value="1"
          label="Option 1"
          // checked={value === '1'}
        />
        <InputRadio
          // onChange={updateRadio}
          name="myRadio"
          value="2"
          label="Option 2"
          // checked={value === '2'}
        />
        <Switch id="test" disabled={false} />
        <Switch id="test1" disabled={true} />
      </Component>

      <Component title="Tabs">
        <Tabs data={tabData} onChangeTab={handleChangeTab} />
      </Component>
    </>
  );
}
