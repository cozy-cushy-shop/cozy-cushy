"use client";
import { ADMIN_APP_ROUTERS } from "helpers/config";
import { redirect } from "next/navigation";
export default function NotFound() {
  redirect(ADMIN_APP_ROUTERS.DASHBOARD.INDEX);
}
