import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";

// This function can be marked `async` if using `await` inside
export function middleware(request: NextRequest) {
  //   const isLogin = request.cookies.has('token');
  //   if (!isLogin) {
  //     return NextResponse.rewrite(new URL(APP_ROUTERS.AUTH, request.url));
  //   }
  return NextResponse.next();
}

export const config = {
  matcher: ["/((?!api|_next/static|_next/image|favicon.ico).*)"],
};
